//
//  ContentView.swift
//  WorldWondersSwiftUI
//
//  Created by Olzhas Akhmetov on 14.04.2021.
//

import SwiftUI
import SwiftyJSON
import SDWebImageSwiftUI

struct WonderRow: View {
    var wonder: WonderData

    var body: some View {
        if wonder.picture != "" {
            WebImage(url: URL(string: wonder.picture))
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 70.0, height: 70.0)
                //.border(Color.red, width: 1)
                .clipped()
                .cornerRadius(6)
        }
        VStack(alignment: .leading, spacing: 4) {
            Text(wonder.name)
            Text(wonder.region)
            
            HStack {
                if wonder.flag != "" {
                    WebImage(url: URL(string: wonder.flag))
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 20, height: 20)
                }
                Text(wonder.location)
            }
            Spacer()
        }
        .padding(.leading, 10.0)
    }
}

struct ContentView: View {
    @ObservedObject var wondersList = GetWonders()
    
    var body: some View {
        
        if !wondersList.isComplete {
            ProgressView()
        }
        
        NavigationView {
            List(wondersList.wondersArray) { wonderItem in
                WonderRow(wonder: wonderItem)
            }
            .navigationTitle("World Wonders")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

//MARK: API Integration.
struct WonderData: Identifiable {
    var id = UUID()
    var name: String!
    var region: String!
    var location: String!
    var flag: String!
    var picture: String!
    
    init() {
        name = ""
        region = ""
        location = ""
        flag = ""
        picture = ""
    }
    
    init(json: JSON) {
        if let temp = json["name"].string {
            name = temp
        }
        if let temp = json["region"].string {
            region = temp
        }
        if let temp = json["location"].string {
            location = temp
        }
        if let temp = json["flag"].string {
            flag = temp
        }
        if let temp = json["picture"].string {
            picture = temp
        }
    }
}

class GetWonders: ObservableObject {
    @Published var wondersArray = [WonderData]()
    
    @Published var isComplete: Bool = false
    
    init() {
        let sourceUrl = "https://demo3886709.mockable.io/getWonders"
        
        let url = URL(string: sourceUrl)!
        
        let session = URLSession(configuration: .default)
        
        session.dataTask(with: url) {
            (data, _, error) in
            
            if error != nil {
                print((error?.localizedDescription)!)
                return
            }
            
            let json = try! JSON(data: data!)
            if let resultArray = json.array {
                for item in resultArray {
                    let wItem = WonderData(json: item)
                    
                    DispatchQueue.main.async {
                        self.wondersArray.append(wItem)
                    }
                }
            }
            
            DispatchQueue.main.async {
                self.isComplete = true
            }
        }.resume()
    }
    
}
