//
//  WorldWondersSwiftUIApp.swift
//  WorldWondersSwiftUI
//
//  Created by Olzhas Akhmetov on 14.04.2021.
//

import SwiftUI

@main
struct WorldWondersSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
